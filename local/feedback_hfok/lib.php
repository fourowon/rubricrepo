<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of functions for the feedback plug-in
 *
 * @package     local
 * @subpackage  feedback_hfok
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!function_exists('get_comment_serials_from_form')){

	function get_comment_serials_from_form() {
		global $CFG, $DB;

		$grp_count = 0;
		$chkboxgrps = 'grp'.$grp_count;
		$table = 'comments';
		$comment_array = array();

		while (!empty($_POST[$chkboxgrps])) {
			//echo 'Checkboxes selected<br>';
			$checkbox = $_POST[$chkboxgrps];
			//echo $selected.'<br>';
			foreach($checkbox as $c) {
				
				// Ignore hidden elements and false advcheckbox elements
				if ($c != 0) {
					$result = $DB->get_record($table, array('id'=>$c));
					array_push($comment_array, $result);
					//echo $result->comment_text.'<br>';
				}
			}
			$grp_count++;
			$chkboxgrps = 'grp'.$grp_count;
		}
		return $comment_array;
	}
}

if (!function_exists('save_comment_serials')){

	function save_comment_serials() {
		global $CFG, $DB, $USER;

		$table1 = 'saved_form';
		$table2 = 'saved_category';
		$table2 = 'saved_comment';
		$table4 = 'saved_freetxt';
		$post_table = 'forum_posts';
		$comments = get_comment_serials_from_form();

		$new_save = new stdClass();
		$saved_freetxt = new stdClass();
		$saved_category = new stdClass();
		$saved_comment = new stdClass();
		
		$post_info = $DB->get_record($post_table, array('id'=>1));
		$new_save->studentid = $post_info->userid;
		$new_save->postid = $post_info->id;
		$new_save->instructorid = $USER->id; // Not sure about this line.

		echo $new_save->studentid.'<br>';
		echo $new_save->postid.'<br>';
		echo $new_save->instructorid.'<br>';
		//echo $USER->id.'<br>';


		foreach($comments as $c) {
			echo $c->id.' '.$c->comment_text.'<br>';
		}
		return;
	}
}
?>