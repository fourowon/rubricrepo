<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca && Sabrina Gannon sgannon1@ualberta.ca && Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_email_instance extends moodleform{
	function definition(){
	global $CFG, $DB;
	$mform = $this ->_form;
	//set size of the header
    $attributes_heading = 'size="24"';
    $attributes_radio_text = 'size="11"';
	
    //sets the content of the email
	$formid = $DB->get_record('saved_form',array('id'=>$_GET['id']));
    $name = $DB-> get_record('user', array('id'=> $formid->studentid)); //need to get from the db
    $firstname = $name->firstname;

    $greeting = get_string('greeting', 'local_feedback_pranjali');
	$comma = get_string('comma', 'local_feedback_pranjali');
    $thanks = get_string('thanks', 'local_feedback_pranjali');
    $thread = $DB->get_record('saved_form',array('id'=>$_GET['id']));
 
    $forumpost = $DB->get_record('forum_posts',array('id'=>$thread->postid)); 

    $topic = $forumpost->subject; //need to get from db
    $goodStuff = get_string('goodStuff', 'local_feedback_pranjali');
	$body = '<p>'.$greeting.' '.$firstname.','.'</p>'.'<p>'.$thanks.$topic.'.'.'</p>';
	if ($DB->get_records('saved_freetxt', array('savedform_id'=>$_GET['id']))) {
        $freetxt = $DB->get_record('saved_freetxt', array('savedform_id'=>$_GET['id']));
        $body = $body.'<p>'.$freetxt->comment_txt.'</p>';
    }

    $body = $body.'<p>'.$goodStuff.'</p>';
	 
    $savedCategories = $DB->get_records('saved_category',array('savedform_id'=>$_GET['id']));
    foreach ($savedCategories as $s) {
        //echo $s->id;
        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
        $categoryName = $categoryRow->name;
        
        if($categoryRow->posneg == 0){
            $body = $body.'<ul>'.'<li>'.$categoryName.'<ul>';
        $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
            foreach($commentRow as $c){
                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                $body = $body.'<li>'.$comment->comment_text.'</li>';
            }
            $body = $body.'</ul>'.'</ul>'.'</li>';

            }
        }
		$body = $body.'</ul>';
		
		$badStuff = get_string('badStuff', 'local_feedback_pranjali');
		$body= $body.'<p>'.$badStuff.'</p>';
		
		foreach ($savedCategories as $s) {
        //echo $s->id;
        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
        $categoryName = $categoryRow->name;
        if($categoryRow->posneg == 1){
            $body = $body.'<ul>'.'<li>'.$categoryName.'<ul>';
			$commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
            foreach($commentRow as $c){
                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                $body = $body.'<li>'.$comment->comment_text.'</li>';
            }
            $body = $body.'</ul>'.'</ul>'.'</li>';
        }
    }
	$body = $body.'</ul>';
	$finalWords =  get_string('finalWords', 'local_feedback_pranjali');
    $markerName = $USER->firstname; //from Sabrina Gannon
    $lastname = $USER->lastname;
	$body = $body .'<p>'.$finalWords .'</p>'.'<p>'.$markerName.' '.$lastname.'</p>';
	$mform->addElement('editor', 'content', null)->setValue(array('text'=>$body));
    $mform->setType('content', PARAM_RAW);
    //adds in the submit and cancel buttons.
    $this->add_action_buttons($cancel=true, $submitlabel = get_string('sendEmail', 'local_feedback_pranjali'));//Courtsey of Henry Fok
	}
};
 
?>
