<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles the logic for the email template
 *
 * @package     local
 * @subpackage  feedback_pranjali
 * @copyright   Pranjali Pokharel pranjali@ualberta.ca && Sabrina Gannon sgannon1@ualberta.ca && Kieran Boyle kboyle@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/**
 * From http://stackoverflow.com/questions/24617350/how-to-create-a-custom-form-in-moodle
 * Credit to: Hipjea
 * Retrieved: Oct. 15, 2016
 */

require_once $CFG->dirroot.'/lib/formslib.php';
/*
* This function creates and displays the email form
* It also fills out predefined feedback snippets for the user to enter
* this functionality will be further refined
*/
class create_email_instance extends moodleform{
	function definition(){
	global $CFG, $DB;
    
    //adds in the submit and cancel buttons.
    $this->add_action_buttons($cancel=true, $sumitlabel = get_string('sendEmail', 'local_feedback_pranjali'));//Courtsey of Henry Fok
	}
    
};
/*
* This function displays the email in a nice preview box from which the instructor can edit or send the email
*/
function display_email(){
    
    global $CFG, $DB;
    //sets the content of the email
	$formid = $DB->get_record('saved_form',array('id'=>$_GET['id']));
    $savedCategories = $DB->get_records('saved_category',array('savedform_id'=>$_GET['id']));
    
    $name = $DB-> get_record('user', array('id'=> $formid->studentid)); //need to get from the db
    $firstname = $name->firstname;
    $lastname = $name->lastname;
    
    $greeting = get_string('greeting', 'local_feedback_pranjali');
	$comma = get_string('comma', 'local_feedback_pranjali');
    $thanks = get_string('thanks', 'local_feedback_pranjali');
    $thread = $DB->get_record('saved_form',array('id'=>$_GET['id']));
 
    $forumpost = $DB->get_record('forum_posts',array('id'=>$thread->postid)); 

    $topic = $forumpost->subject; //need to get from db
    $goodStuff = get_string('goodStuff', 'local_feedback_pranjali');
    $badStuff = get_string('badStuff', 'local_feedback_pranjali');
    $finalWords =  get_string('finalWords', 'local_feedback_pranjali');
    //STILL NEED TO GET INSTRUCTOR's NAME
    echo '<h2 style= "float:center; text-align:center;">Forum feedback has been processed and saved. Would you like to send it as an email? </h2><br>';
    echo '<div class = "forumpost clearfix read firstpost starter">'; //full forum post
		echo '<div class = "row header clearfix">' ; //the grey shaded header bit
			echo '<div class = "topic firstpost starter">';
				echo '<div class = "subject" role = "heading">';
					echo 'Subject: '.'Feedback for - '.$topic.'<br>';
					echo "</div>";
	
				echo '<div class = "author" role="heading" >';
					echo 'Author: '.$firstname.' '.$lastname.'<br>';
					echo "</div>";
				echo "</div";
			echo "</div>"; //end topic firstpost starter
		echo "</div>"; //end row header clearfix -- grey shaded header bit --end of header
	
		echo '<div class= "row maincontent clearfix">';
			echo '<div class ="no overflow" style ="padding-left:40px">';
				echo '<div class= "content">'; //discussion post content ie message
					echo'<br>';
					echo $greeting.' '.$firstname.$comma.'<br><br>';
                    echo $thanks.' '.$topic.'.<br><br>';
                    
                    echo $goodStuff.'<br>';
                    foreach ($savedCategories as $s) {
                        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
                        $categoryName = $categoryRow->name;
                        /*
                         * Tab in HTML http://stackoverflow.com/questions/1571648/html-tab-space-instead-of-multiple-non-breaking-spaces-nbsp
                         * Author: James Donnelly
                         * Accessed: November 25, 2015 by Pranjali
                         */
                        if($categoryRow->posneg == 0){
                            echo '&emsp;-'.$categoryName.'<br>';
                            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
                            foreach($commentRow as $c){
                                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                                echo '&emsp;&emsp;&emsp;-'.$comment->comment_text.'<br>';
                            }
                        }
                    }
                    
                    echo '<br>'.$badStuff.'<br>';
                    foreach ($savedCategories as $s) {
                        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
                        $categoryName = $categoryRow->name;
                        if($categoryRow->posneg == 1){
                            echo '&emsp;-'.$categoryName.'<br>';
                            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
                            foreach($commentRow as $c){
                                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                                echo '&emsp;&emsp;&emsp;-'.$comment->comment_text.'<br>';
                            }
                        }
                    }
                    
                    echo '<br><br>'.$finalWords.'<br>';
                    //ADD INSTRUCTOR'S NAME OR SIGNATURE HERE!
                    echo '<br><br>';
                echo "</div>";
            echo "</div>";
        echo "</div>";
        
        /*
         ************* BOTTOM OPTIONS IN THE BOX -- EDIT OPTION *****************
         */
        
        echo '<div class="row side">';
            echo '<div class="left">&nbsp;</div>';
                echo '<div class="options clearfix">';
                    echo '<div class="commands">';
                        echo '<a href="'.$CFG->wwwroot.'/local/feedback_pranjali/email.php?id='.$_GET['id'].'">Edit</a>'; 
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        echo '</div>';
    echo "</div>";
 
}
?>
