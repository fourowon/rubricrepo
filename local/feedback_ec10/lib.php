<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of functions for the feedback plug-in
 *
 * @package     local
 * @subpackage  feedback_ec10
 * @copyright   Eric Cheng ec10@ualberta.ca
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


/**
 * This function extracts the value from the selected checkboxes ONLY.  The value returned for selected checkboxes
 * is the ID number of the corresponding comment for the feedback form.  If checkbox is not selected, the default
 * return value for the checkbox is 0, which is ignored.
 *
 * @return An array of comment objects.
 */
function get_comment_serials_from_form() {
	global $CFG, $DB;

	$grp_count = 0;
	$chkboxgrps = 'grp'.$grp_count;
	$table = 'comments';
	$comment_array = array();

	while (!empty($_POST[$chkboxgrps])) {
		//echo 'Checkboxes selected<br>';
		$checkbox = $_POST[$chkboxgrps];
		//echo $selected.'<br>';
		foreach($checkbox as $c) {
			
			// Ignore hidden elements and false advcheckbox elements
			if ($c != 0) {
				$result = $DB->get_record($table, array('id'=>$c));
				array_push($comment_array, $result);
				//echo $result->comment_text.'<br>';
			}
		}
		$grp_count++;
		$chkboxgrps = 'grp'.$grp_count;
	}
	return $comment_array;
}

/**
 * Saves the completed feedback form to the database.  Fields must not be empty when saving to prevent rows from being empty in the db.
 *
 * @return The ID of the saved form.
 */
function save_completed_form() {
	global $CFG, $DB, $USER;
	require_once($CFG->dirroot.'/lib/gradelib.php');
	$table1 = 'saved_form';
	$table2 = 'saved_category';
	$table3 = 'saved_comment';
	$table4 = 'saved_freetxt';
	$table5 = 'forum_posts';

	$comments = get_comment_serials_from_form();
	$threadid = $_POST['threadid'];
	$thread_info = $DB->get_record($table5, array('id'=>$threadid)); // We need the thread info in order to get the student ID

	$new_save = new stdClass();
	$saved_freetxt = new stdClass();
	$saved_category = new stdClass();
	$saved_comment = new stdClass();
	
	$new_save->postid = $threadid;
	$new_save->instructorid = $USER->id;
	$new_save->studentid = $thread_info->userid;
	
	$savedform_id = $DB->insert_record($table1, $new_save); // By default, the ID of the new inserted record is returned.

	if($_POST['freetextcmt']) { // Save the comment in the free text box only if it is not empty.
		$saved_freetxt->savedform_id = $savedform_id;
		$saved_freetxt->comment_txt = $_POST['freetextcmt'];
		//$DB->insert_record($table4, $saved_freetxt);
	}
	//var_dump($_POST['freetextcmt']);
	//var_dump($_POST['scale']);	

	   foreach($comments as $c) {
                $saved_category->savedform_id = $savedform_id;
                $saved_category->category_ref_id = $c->category;
                if(!$DB->get_record($table2, array('savedform_id'=>$savedform_id, 'category_ref_id'=>$c->category))) { //If the category for this saved form doesn't exist yet, 

                                      //then create the entry for it.
                        $savedcategory_id = $DB->insert_record($table2, $saved_category);
                }
                $saved_comment->savedcategory_id = $savedcategory_id;
                $saved_comment->comment_ref_id = $c->id;
                $DB->insert_record($table3, $saved_comment);
        }
                   /*--------------------------------------------------------------------------- */
                        /*----------------------------Testing updating assignment grade -------------i*/
    if ($_POST['scale'] && $_POST['assign_grade']) { // Only update the grade book if a grade has been given AND an assignment has been selected.
	   	$grade_input = $_POST['scale'];
	    $assign_value = $_POST['assign_grade'];
	    //$forum_discussion = $DB->get_record('forum_discussions',array('firstpost'=>$_GET['id']));
	   //	$courseid = $forum_discussion->course;
	   //	$params = array('idnumber'=>$thread_info->id);
	   // grade_update('local/feedback_ec10', $courseid, 'local', 'feedbackplugin', $assign_value, 0, $grade_input, $params);
	    //echo 'Can assign grades.  User is:'.$thread_info->userid.' and assignment is: '.$assign_value.'<br>';
	    //$grade_value = $feedback_form->scale;
	    //$forum_discussion = $DB->get_record('forum_discussions',array('firstpost'=>$_GET['id']));



   
   	$grade = new stdClass();
    	$grade->userid = $thread_info->userid;
    	$grade->timecreated = time();
    	$grade->timemodified = time();
    	$grade->grader = $USER->id;
    	$grade->assignment = $assign_value; //Need to grab assignment selection from user
    	$grade->grade = $grade_input;
    	//var_dump($grade);
	    //$grade->grade = 85;  //Need to grab assignment grade from user
	    $checkIfGraded = $DB->get_record('assign_grades',array('userid'=>$grade->userid, 'assignment'=>$grade->assignment));
	    if($checkIfGraded){
	    	$checkIfGraded->grade = $grade_input;
	    	$DB->update_record('assign_grades', $checkIfGraded);
	    } else {
	        $DB->insert_record('assign_grades', $grade);
	    }	   //}


 	   $savedCategories = $DB->get_records('saved_category',array('savedform_id'=>$saved_category->savedform_id));
	   $finalWords =  get_string('finalWords', 'local_feedback_ec10');
	   $goodstuff = get_string('goodstuff','local_feedback_ec10');
	   $badstuff = get_string('badstuff','local_feedback_ec10');

	   $forum_discussion = $DB->get_record('forum_discussions',array('firstpost'=>$_GET['id']));
	   $courseid = $forum_discussion->course;

	   $itemid = $DB->get_record('grade_items',array('iteminstance'=>$grade->assignment, 'courseid'=>$courseid));
	   //echo $itemid->id.'<br>';
	   $student = new stdClass();
	   $student->userid = $thread_info->userid;
	   $student->rawgrade = $grade_input;
	   $student->usermodified = $USER->id;
	   $student->datesubmitted = time();
           $student->dategraded = $grade->timemodified;
	  
	//Used to populate feedback comment with instructors response (code taken from email_preview_form.php)
	//This was done in two places
	 $student->feedback = "<p><b>Title:</b></p><br>".$thread_info->subject."<br><br><p><b>Good Stuff:</b>";


		foreach ($savedCategories as $s) {
                        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
                        $categoryName = $categoryRow->name;
                        if($categoryRow->posneg == 0){
                            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
                            foreach($commentRow as $c){
                                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
				$student->feedback = $student->feedback."<br>".$comment->comment_text."<br>";
                            }
                        }
                    }

		$student->feedback = $student->feedback." <br><p><b>Bad Stuff:</b></p>";


		 foreach ($savedCategories as $s) {
                        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
                        $categoryName = $categoryRow->name;
                        if($categoryRow->posneg == 1){
                            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
                            foreach($commentRow as $c){
                                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                          	$student->feedback = $student->feedback."<br>".$comment->comment_text."<br>"; 
			   }
                        }
                    }

	  	$student->feedback = $student->feedback."</p><br><br><p><b> Final Comments:</b></p>".$_POST['freetextcmt']."<br><br>".$finalWords;
 
	
	   //$params = array('itemname'=> 'feedback', 'idnumber'=>$thread_info->userid, 'gradetype'=>GRADE_TYPE_VALUE);
	   //$ret = grade_update('local/feedback_ec10', $itemid->courseid, 'local', 'feedback_ec10', $itemid->id, 0, $student);
	
	  //Used to adjust students grade, feedback etc. 
	  $ret = grade_update('mod/assign', $itemid->courseid, 'mod', 'assign', $itemid->iteminstance, 0, $student);
	  

	//Second place we need to populate the assignment feedback
	   $assignfeedback = $DB->get_record('assignfeedback_comments',array('assignment'=>$checkIfGraded->assignment, 'grade'=>$checkIfGraded->id)); 
	   $assignfeedback->commenttext ="<p><b>Title:</b></p><br>".$thread_info->subject."<br><br><p><b>Good Stuff:</b>";

	   

	foreach ($savedCategories as $s) {
                        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
                        $categoryName = $categoryRow->name;
                        if($categoryRow->posneg == 0){
                            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
                            foreach($commentRow as $c){
                                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
				$assignfeedback->commenttext = $assignfeedback->commenttext."<br>".$comment->comment_text."<br>";
                            }
                        }
                    }


	   $assignfeedback->commenttext = $assignfeedback->commenttext."</p><br><br><p><b>Bad Stuff:</b>";



	 foreach ($savedCategories as $s) {
                        $categoryRow = $DB->get_record('category',array('id'=>$s->category_ref_id));
                        $categoryName = $categoryRow->name;
                        if($categoryRow->posneg == 1){
                            $commentRow = $DB->get_records('saved_comment',array('savedcategory_id'=>$s->id));
                            foreach($commentRow as $c){
                                $comment = $DB -> get_record('comments',array('id'=>$c->comment_ref_id));
                          	$assignfeedback->commenttext = $assignfeedback->commenttext."<br>".$comment->comment_text."<br>"; 
			   }
                        }
                    }



	  $assignfeedback->commenttext = $assignfeedback->commenttext."</p><br><br><p><b> Final Comments:</b></p>".$_POST['freetextcmt']."<br><br>".$finalWords;
 

	  //echo $checkIfGraded->assignment."<br>i<br>";
	  //echo $checkIfGraded->id;
	 //echo "<br>".$assignfeedback->id."<br><br>";
		

	  //Checks to see if a feedback for that assignment is already present or not.
	   if ($assignfeedback->id != NULL) {
	   	$DB->update_record('assignfeedback_comments', $assignfeedback);
	   } else {

		
		$feedback = $DB->get_record('assign_grades',array('userid'=>$grade->userid, 'assignment'=>$grade->assignment));
		$assignfeedback->assignment = $feedback->assignment;
		$assignfeedback->grade = $feedback->id;
		$DB->insert_record('assignfeedback_comments', $assignfeedback);

	
	    }

		//  $studentgrade = $DB->get_record('grade_grades', array('userid'=>$grade->userid, 'itemid'=>$itemid->iteminstance));
	   //echo $studentgrade->id.'<br>';
          // $studentgrade->rawgrade = $grade_input;
	  // $studentgrade->finalgrade = $grade_input;
	  // $studentgrade->feedback = "perfect!";
	  // $studentgrade->userid = $grade->userid;
	   //$studentgrade->itemid = $itemid->item; 
	  // $studentgrade->id = USERID;	
    	 //  $DB->update_record('grade_grades', $studentgrade);
	   //$DB->set_field('grade_grades', $studentgrade,);
	  // echo $ret;
	}
	    /*
	    $forum_discussion = $DB->get_record('forum_discussions',array('firstpost'=>$_GET['id']));
	   	$courseid = $forum_discussion->course;
	    $itemid = $DB->get_record('grade_items',array('iteminstance'=>$grade->assignment, 'courseid'=>$courseid));
	    $assignment = $DB->get_record('assign', array('id'=>$assign_value));
	    //var_dump($itemid);
	    //echo '<br>';
	    //if ($DB->get_record('grade_grades',array('userid'=>$grade->userid, 'itemid'=>$itemid->id))) {
	    //	$student = $DB->get_record('grade_grades',array('userid'=>$grade->userid, 'itemid'=>$itemid->id));
	    $grade_book = new stdClass();
	    $grade_book->itemid = $itemid;
	    $grade_book->userid = $thread_info->userid;
	    $grade_book->rawgrade = $grade_input;
	    $grade_book->rawgrademax = $assignment->grade;
	    $grade_book->rawgrademin = 0;
	    //$grade_book->rawscaleid = NULL;
	    $grade_book->usermodified = $USER->id;
	    $grade_book->finalgrade = $grade_input;
	    //$grade_book->hidden = 0;
	    //$grade_book->locked = 0;
	    //$grade_book->locktime = 0;
	    //$grade_book->export = 0;
	    //$grade_book->overriden = 0;
	    //$grade_book->excluded = 0;
	    //$grade_book->feedback = NULL;
	    //$grade_book->feedbackformat = 0;
	    //$grade_book->information = NULL;
	    //$grade_book->informationformat = 0;
	    //$grade_book->timecreated = time();
	    //$grade_book->timemodified = time();
	    $DB->insert_record('grade_grades', $grade_book);
	    // Assigning grades at this point, need t be able to grab user input information.
	    	//$student->rawgrade = $grade_input;
	    	//$student->finalgrade = $grade_input;
	    	//var_dump($student);
	    	//$DB->update_record('grade_grades',$student);
	}
	*/


/*	foreach($comments as $c) {
		$saved_category->savedform_id = $savedform_id;
		$saved_category->category_ref_id = $c->category;
		if(!$DB->get_record($table2, array('savedform_id'=>$savedform_id, 'category_ref_id'=>$c->category))) { //If the category for this saved form doesn't exist yet, 
																											   //then create the entry for it.
			//$savedcategory_id = $DB->insert_record($table2, $saved_category);
		}
		$saved_comment->savedcategory_id = $savedcategory_id;
		$saved_comment->comment_ref_id = $c->id;
		//$DB->insert_record($table3, $saved_comment);
	} */

	return $savedform_id;
}
?>
