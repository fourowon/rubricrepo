INSERT INTO mdl1_feedback_form VALUES(0,"branchMueller",null);

INSERT INTO mdl1_category VALUES(0,0,"Writing",null);
INSERT INTO mdl1_category VALUES(1,1,"Writing",null);
INSERT INTO mdl1_category VALUES(2,0,"Connections",null);
INSERT INTO mdl1_category VALUES(3,1,"Connections",null);
INSERT INTO mdl1_category VALUES(4,0,"Engaging and Inviting",null);
INSERT INTO mdl1_category VALUES(5,1,"Engaging and Inviting",null);
INSERT INTO mdl1_category VALUES(6,0,"Meets Assignment Expectations",null);
INSERT INTO mdl1_category VALUES(7,1,"Meets Assignment Expectations",null);

INSERT INTO mdl1_comments VALUES(0,0,"Writes creatively");
INSERT INTO mdl1_comments VALUES(1,0,"Presents ideas thoughtfully");
INSERT INTO mdl1_comments VALUES(2,0,"Writes clearly and concisely");
INSERT INTO mdl1_comments VALUES(3,0,"Writes correctly (free of spelling and grammatical errors)");
INSERT INTO mdl1_comments VALUES(4,0,"Organizes scholarly contribution");
INSERT INTO mdl1_comments VALUES(5,0,"Synthesizes of assigned reading/resource");
INSERT INTO mdl1_comments VALUES(6,0,"Uses few direct quotes");
INSERT INTO mdl1_comments VALUES(7,0,"Presents of unique ideas/conceptualizations");

INSERT INTO mdl1_comments VALUES(8,1,"Engage the reader with a story, metaphor or analogy");
INSERT INTO mdl1_comments VALUES(9,1,"Organize the post into clear themes");
INSERT INTO mdl1_comments VALUES(10,1,"Demonstrate synthesis of all assigned readings/resources");
INSERT INTO mdl1_comments VALUES(11,1,"Paraphrase or cite more often and use direct quotes less often");
INSERT INTO mdl1_comments VALUES(12,1,"Check your grammar and spelling (watch for typos)");
INSERT INTO mdl1_comments VALUES(13,1,"Use a more academic tone");


INSERT INTO mdl1_comments VALUES(14,2,"Makes appropriate connections to assigned readings/resources");
INSERT INTO mdl1_comments VALUES(15,2,"Makes clear connections to other research (scholarly articles, conference proceedings)");
INSERT INTO mdl1_comments VALUES(16,2,"Makes appropriate connections to other resources (books, curriculum, documents, school policies, websites, reports, etc.)");
INSERT INTO mdl1_comments VALUES(17,2,"Includes other sources of practical expertise (blogs, wikis, videos, etc.)");
INSERT INTO mdl1_comments VALUES(18,2,"Makes clear connections to personal experiences");
INSERT INTO mdl1_comments VALUES(19,2,"Makes clear connections to professional experiences");


INSERT INTO mdl1_comments VALUES(20,3,"Make connections to all assigned readings/resources");
INSERT INTO mdl1_comments VALUES(21,3,"Search for research related to the topic to enhance your scholarly contribution");
INSERT INTO mdl1_comments VALUES(22,3,"Include links to other resources (books, curriculum documents, school policies, websites, reports, etc)");
INSERT INTO mdl1_comments VALUES(23,3,"Make clear connections to personal experiences");
INSERT INTO mdl1_comments VALUES(24,3,"Make clear connections to professional experiences");

INSERT INTO mdl1_comments VALUES(25,4,"Invites others to make connections");
INSERT INTO mdl1_comments VALUES(26,4,"Asks questions to promote discussion");
INSERT INTO mdl1_comments VALUES(27,4,"Poses problems or challenges to enhance discussions");

INSERT INTO mdl1_comments VALUES(28,6,"Meets word limit");
INSERT INTO mdl1_comments VALUES(29,6,"Uses in-text citatiions correctly (in APA)");
INSERT INTO mdl1_comments VALUES(30,6,"Includes a complete and correctly formatted reference list (in APA)");

INSERT INTO mdl1_comments VALUES(31,7,"Keep to the word limit");
INSERT INTO mdl1_comments VALUES(32,7,"Use your APA manual to help you conplete correctly formatted in-text citations");
INSERT INTO mdl1_comments VALUES(33,7,"Use your APA manual to help you complete a correctly formatted reference list");